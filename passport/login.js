var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var configurations = require("../config");
//var FCM = require("../utils/fcm");

module.exports = function (passport) {
    passport.use('login', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, username, password, done) {
        User.findOne({
            'username': username,
        }, function (err, user) {
            // In case of any error, return using the done method
            if (err) {
                return done({ error: err });
            }
            // Username does not exist, log the error and redirect back
            if (!user) {
                console.log('User Not Found with username ' + username);
                return done(null, false, { 'message': 'User Not found.' });
            }
            // User exists but wrong password, log the error
            if (!isValidPassword(user, password)) {
                console.log('Invalid Password');
                return done(null, false, { 'message': 'Invalid Password' }); // redirect back to login page
            }
            if (user.email_verified==false) {
                console.log('Account is not activated' + username);
                return done(null, false, { 'message': 'Account.' });
            }
    
            user.expiresAt = null;
            user.jwt_token = "";
            var jwt_object = {};
            jwt_object.username = user.username;
            jwt_object.user_id = user.user_id;

            var jwt_token = jwt.sign({
                data: jwt_object,
                exp: Math.floor(Date.now() / 1000) + (2 * 60 * 60),
            }, configurations.TOKEN_SECRET);
            user.jwt_token = jwt_token;
            user.is_active = true;

            ({
                'username': username
            }, function (err, users) {
                if (users) {
                    return done(null, {
                        jwt_token: user.jwt_token,
                        username: user.username,
                        user_id: user.user_id,
                        user_role: user.user_role,
                        fcm_token: user.fcm_token,
                        isLoggedin: true
                    });
                }
            });
            user.save({
                'username': username
            }, function (err, users) {
                if (users) {
                    return done(null, {
                        jwt_token: user.jwt_token,
                        username: user.username,
                        user_id: user.user_id,
                        user_role: user.user_role,
                        fcm_token: user.fcm_token,
                        isLoggedin: true
                    });
                }
            });
            // var FCM_TOKEN={
            //     token:user.fcm_token
            // };
            // FCM.FCM_notification(FCM_TOKEN, (error, info) => {
            //     if (error) {
            //         return console.log(error);
            //     }
            //     console.log('Message %s sent: %s', info.messageId, info.response);
            //     res.render('index');
            // });
        });
    })
    );

    // Utility Function
    var isValidPassword = function (user, password) {
        return bCrypt.compareSync(password.toString(), user.password);
    }

};
