var Category = require("../models/Categories");
var Product = require("../models/products");

module.exports = {

    add_category: function (data, callback) {
        eventdata = function () {
            Category.findOne({
                "category_id": data.category_id
            }, function (err, categorydata) {
                if (err) {
                    callback({
                        statuscode: 500,
                        error: err,
                        msg: "finding category_id database error"
                    });
                } else if (categorydata != null) {
                    callback({
                        statuscode: 304,
                        msg: "Category already exist"
                    });

                } else {
                    var addcategory = new Category();
                    addcategory.category_id = data.category_id;
                    addcategory.category_name = data.category_name;
                    addcategory.product_id = data.product_id;
                    addcategory.category_des = data.category_des;
                    addcategory.save(function (err, result) {
                        if (err) {
                            callback({
                                statuscode: 500,
                                error: err,
                                msg: "saving category, database error"
                            });
                        } else {
                            callback({
                                statuscode: 200,
                                msg: "Category added successfully",
                                data: result
                            });
                        }
                    });
                }
            });
        }
        process.nextTick(eventdata);
    },

    list_categories: function (callback) {
        categoryData = function () {
            Category.aggregate([
                {
                    "$lookup": {
                        "from": "products", //
                        "localField": "product_id",
                        "foreignField": "product_id",
                        "as": "products"
                    }
                }
            ], function (err, categories) {
                if (err) {
                    callback({
                        msg: "Finding categories from database , an error",
                        statuscode: 500,
                    });
                } else if (categories == null) {
                    callback({
                        msg: "No evcategoriesents found",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "List of categories",
                    statuscode: 200,
                    data: categories
                });
            });
        }
        process.nextTick(categoryData);
    },

    get_category: function (data, callback) {
        categoryData = function () {
            Category.aggregate([
                { "$match": { "category_id": data.category_id } },
                {
                    "$lookup": {
                        "from": "products",
                        "localField": "product_id",
                        "foreignField": "product_id",
                        "as": "products"
                    }
                }
            ], function (err, categories) {
                if (err) {
                    callback({
                        msg: "Finding categories from database , an error",
                        statuscode: 500,
                    });
                } else if (categories == null) {
                    callback({
                        msg: "No evcategoriesents found",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "List of categories",
                    statuscode: 200,
                    data: categories
                });
            });
        }
        process.nextTick(categoryData);
    }
}
