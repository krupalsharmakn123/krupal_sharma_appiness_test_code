var Product = require("../models/products");

module.exports = {

    add_product: function (data, callback) {
        productdata = function () {
            Product.findOne({
                "product_id": data.product_id
            }, function (err, productdata) {
                if (err) {
                    callback({
                        statuscode: 500,
                        error: err,
                        msg: "finding product_id database error"
                    });
                } else if (productdata != null) {
                    callback({
                        statuscode: 304,
                        msg: "Product already exist"
                    });

                } else {
                    var addproduct = new Product();
                    addproduct.product_id = data.product_id;
                    addproduct.product_name = data.product_name;
                    addproduct.product_des = data.product_des;
                    addproduct.save(function (err, result) {
                        if (err) {
                            callback({
                                statuscode: 500,
                                error: err,
                                msg: "saving Product, database error"
                            });
                        } else {
                            callback({
                                statuscode: 200,
                                msg: "Product added successfully",
                                data: result
                            });
                        }
                    });

                }
            });
        }
        process.nextTick(productdata);
    },

    get_product: function (data, callback) {
        productData = function () {
            Product.findOne({
                "product_id": data.product_id
            }, function (err, product) {
                if (err) {
                    callback({
                        msg: "Finding product_id from database , an error",
                        statuscode: 500,
                    });
                } else if (product == null) {
                    callback({
                        msg: "product_id does not exist",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "product details",
                    statuscode: 200,
                    data: product
                });
            });
        }
        process.nextTick(productData);
    },

    list_products: function (callback) {
        productData = function () {
            Product.find({}, function (err, products) {
                if (err) {
                    callback({
                        msg: "Finding products from database , an error",
                        statuscode: 500,
                    });
                } else if (products == null) {
                    callback({
                        msg: "No products found",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "List of products",
                    statuscode: 200,
                    data: products
                });
            });
        }
        process.nextTick(productData);
    }
}
