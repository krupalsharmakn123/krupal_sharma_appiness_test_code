var bCrypt = require('bcrypt-nodejs');
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var BASE_API_URL = "";
var version = "1.0"; // version code

/* congig */
var configuration = require("../../config");

/* utils*/

/* handler */
var ProductHandler = require("../../handlers/product_handler");
var CategoryHandler = require("../../handlers/category_handler");

/* Generates hash using bCrypt*/
var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

/* Authentication function*/
var isAuthenticatedAccessToken = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers.authorization;
    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, configuration.TOKEN_SECRET, function (err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other
                req.user = decoded;
                next();
            }
        });
    } else {
        return res.status(403).send({
            statuscode: 203,
            msgkey: "api.access.token.failed",
            v: version
        });
    }
}

module.exports = function (passport) {

    /*API to accress root*/
    router.get(BASE_API_URL + '/', function (req, res) {
        var reponseJson = {
            statuscode: 200,
            msgkey: "login.first.to.access.api",
            v: version
        };
        res.json(reponseJson);
    });

    router.post(BASE_API_URL + '/add_product', function (req, res) {
        var data = req.body;
        ProductHandler.add_product(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_product', function (req, res) {
        var data = req.query;
        ProductHandler.get_product(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_products', function (req, res) {
        ProductHandler.list_products(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/add_category', function (req, res) {
        var data = req.body;
        CategoryHandler.add_category(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_category', function (req, res) {
        var data = req.query;
        CategoryHandler.get_category(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_categories', function (req, res) {
        CategoryHandler.list_categories(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    return router;
};
