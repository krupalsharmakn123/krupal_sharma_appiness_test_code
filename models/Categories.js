var mongoose = require('mongoose');
module.exports = mongoose.model('Categories', {
    category_id: String,
    category_name: String,
    rating: String,
    product_id: [],
    category_des:String,
    created_at: {type: Date, default: Date.now},
    isActive: { type:Boolean, default:true}
});
